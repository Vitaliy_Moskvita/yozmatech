import { Tab } from "../types/types"

type Props = {
    onChange: (tab: Tab) => void,
    activeTab: Tab
};

const Tabs: Tab[] = [Tab.Tracker, Tab.List];

export const MainTabs = ({ onChange, activeTab }: Props) => {
    return (
        <div className="col">
            <div className="tabs section-item">
                <ul className="nav nav-pills nav-fill">
                    {Tabs.map((tab: Tab, i: number) =>
                        <li key={i} className="nav-item">
                            <div className={`nav-link ${tab === activeTab ? 'active' : ''}`} onClick={() => onChange(tab)} >
                                {tab}
                            </div>
                        </li>
                    )}
                </ul>
            </div>
        </div>
    )
}