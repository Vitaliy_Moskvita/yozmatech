import React, { useState } from "react";
import { TrackerData, User } from "../types/types";

type Props = { users: User[], onSave: (data: TrackerData) => void };
const defaultValue: TrackerData = { userId: '', amountTime: '', notes: '' };

export const Tracker = ({ users, onSave }: Props) => {
    const [data, setData] = useState<TrackerData>(defaultValue);

    const changeUser = (e: React.ChangeEvent<HTMLSelectElement>) => {
        const _data = { ...data, userId: +e.currentTarget?.value };
        setData(_data);
    }

    const changeTime = (e: React.ChangeEvent<HTMLInputElement>) => {
        const _data = { ...data, amountTime: e.currentTarget?.value };
        setData(_data);
    }

    const changeNote = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
        const _data = { ...data, notes: e.currentTarget?.value };
        setData(_data);
    }

    const isFormValid = () => data.userId && data.notes.length > 0 && data.amountTime > 0;

    const saveData = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        if (isFormValid()) {
            onSave(data);
            setData(defaultValue);
        }
    }

    return (
        <div className="col">
            <div className="border-item">
                <form onSubmit={e => saveData(e)}>
                    <div className="mb-3">
                        <label htmlFor="user" className="form-label">User</label>
                        <select required id="user" value={data.userId} onChange={e => changeUser(e)} className="form-select form-select mb-3" aria-label=".form-select-lg example" >
                            <option value=""> select...</option>
                            {users.map((user: User, i: number) =>
                                <option key={i} value={user.id}>{`${user.name} (${user.username})`}</option>
                            )}
                        </select>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="time" className="form-label">Time spend</label>
                        <div className="input-group">
                            <input required type="number" step={0.10} id="time" className="form-control" aria-label="Amount of time" value={data.amountTime} onChange={e => changeTime(e)} />
                            <span className="input-group-text">hours</span>
                        </div>
                    </div>
                    <div className=" mb-3">
                        <label htmlFor="notes" className="form-label">Notes</label>
                        <textarea className="form-control" value={data.notes} id="notes" aria-label="With textarea" onChange={e => changeNote(e)}></textarea>
                    </div>
                    <button type="submit" className="btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    )
}