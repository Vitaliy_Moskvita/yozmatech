import { Link } from "react-router-dom";
import logo from '../img/yozma.tech-logo.png';

export const Header = () => {
    return (
        <nav className="navbar navbar-light ">
        <div className="container-fluid">
          <Link to={"/"}>
            <img src={logo} alt="" width="30" height="24" className="d-inline-block align-text-top" />
          </Link>
        </div>
      </nav>
    )
}