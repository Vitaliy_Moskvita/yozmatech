/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from 'react';
import { Link } from "react-router-dom";
import { LocalData, TrackerData, User } from "../types/types";
import { LocalDataService } from '../services/localData.service';

type Props = { data: User[] };

export const NotesList = ({ data }: Props) => {
    const [notes, setNotes] = useState<LocalData[]>([]);
    const { getAllLocalData } = LocalDataService();

    useEffect(() => { getNotes() }, []);

    const getNotes = (): void => {
        const _notes = getAllLocalData();
        _notes && setNotes(_notes);
    }

    const getUserById = (id: number): User | undefined => data.find(x => x.id === id);

    return (
        <div className="col">
            <div className="table-responsive">
                <table className="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">User</th>
                            <th scope="col">Duration</th>
                            <th scope="col">Notes</th>
                            <th scope="col" className="align-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {notes.map((userNote: LocalData, i: number) =>
                            userNote.data.map((note: TrackerData, inx) =>
                                <tr key={i + inx} className={`item-list ${(i & 1) ? "odd" : "even"}`}>
                                    <td>{inx === 0 && getUserById(+note.userId)?.name}</td>
                                    <td>{note.amountTime} hours</td>
                                    <td>{note.notes}</td>
                                    <td className="align-center">
                                        <Link to={`/details/${note.userId}`}>
                                            <i className="fa fa-eye" title="View details" aria-hidden="true" />
                                        </Link>
                                    </td>
                                </tr>
                            )
                        )}
                    </tbody>
                </table>
            </div>
        </div>
    )
}