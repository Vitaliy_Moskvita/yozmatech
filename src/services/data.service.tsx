import { RequestProps } from "../types/types";

const baseUrl = "https://jsonplaceholder.typicode.com/";

export const DataService = (api: string) => {
    const URL = `${baseUrl}${api}/`;

    const getData = async (params: RequestProps = {}) => {
        let requestOptions = {
            method: params.method ? params.method : "GET",
            headers: { "Content-Type": "application/json" },
            body: params.body ? JSON.stringify(params.body) : undefined
        }
        try {
            const response = await fetch(`${URL}${params.id ? params.id : ''}`, requestOptions);
            const data = await response.json();
            if (!response.ok) {
                throw (data.code ? data : response.status);
            } else {
                return data;
            }
        } catch (error) {
            throw new Error(error.message);
        }
    }

    const updateData = () => {
        fetch('https://jsonplaceholder.typicode.com/users/1', {
            method: 'PUT',
            body: JSON.stringify({
                id: 1,
                title: 'foo',
                body: 'bar',
                userId: 1,
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        })
            .then((response) => response.json())
            .then((json) => console.log(json));
    }

    return { getData, updateData};
}
