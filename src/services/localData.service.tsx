import { reactLocalStorage } from 'reactjs-localstorage';
import { LocalData, TrackerData } from '../types/types';

const defaultValue: LocalData = { data: [] };

export const LocalDataService = () => {

    const getLocalData = (key: string): LocalData => {
        const res = reactLocalStorage.getObject(key, defaultValue);
        return res as LocalData;
    }

    const setLocalData = (noteData: TrackerData): void => {
        const res = getLocalData(`${noteData.userId}`);
        res.data.push(noteData);
        reactLocalStorage.setObject(`${noteData.userId}`, res);
    }

    const getAllLocalData = ():LocalData[] => {
        const values: LocalData[] = [],
        keys = Object.keys(localStorage);
        keys?.forEach(key => values.push(getLocalData(key)));
        return values;

    }

    return { getLocalData, setLocalData, getAllLocalData };
}