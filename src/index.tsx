import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';
// import { BrowserRouter } from 'react-router-dom';
import App from './App';

import { Router } from "react-router-dom"
import { createBrowserHistory } from 'history'
const history = createBrowserHistory()

ReactDOM.render(
  <React.StrictMode>
    {/* <BrowserRouter> */}
    <Router history={history}>
      <App />
    </Router>
    {/* </BrowserRouter> */}
  </React.StrictMode>,
  document.getElementById('root')
);