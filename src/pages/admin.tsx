/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from "react";
import { NotesList } from "../components/NotesList";
import { MainTabs } from "../components/Tabs";
import { Tracker } from "../components/Tracker";
import { DataService } from "../services/data.service";
import { LocalDataService } from '../services/localData.service';
import { Tab, User } from "../types/types";


export const AdminPage = () => {
    const [users, setUsers] = useState<User[]>([]);
    const [activeTab, setActiveTab] = useState<Tab>(Tab.Tracker);
    const { getData } = DataService("users");
    const { setLocalData } = LocalDataService();

    useEffect(() => { getUsers() }, []);

    const getUsers = async () => {
        const _users = await getData();
        _users && setUsers(_users);
    }

    return (
        <div className="container">
            <div className="row">
                <MainTabs onChange={tab => setActiveTab(tab)} activeTab={activeTab} />
            </div>
            <div className="row section-item">
                {activeTab === Tab.Tracker ?
                    <Tracker users={users} onSave={data => setLocalData(data)} />
                    :
                    <NotesList data={users} />
                }
            </div>
        </div>
    )
}