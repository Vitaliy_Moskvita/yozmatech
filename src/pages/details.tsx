/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from "react";
import { Link } from "react-router-dom"
import { DataService } from "../services/data.service";
import { LocalDataService } from "../services/localData.service";
import { LocalData, TrackerData, User } from "../types/types";


export const DetailsPage = (props: { match: { params: { id: string } } }) => {
    const [note, setNote] = useState<LocalData>({} as LocalData);
    const [user, setUser] = useState<User>({} as User);
    const { getLocalData } = LocalDataService();
    const { getData } = DataService("users");

    useEffect(() => {
        props.match.params?.id && loadData(props.match.params?.id);
    }, [props.match.params]);

    const loadData = async (id: string): Promise<void> => {
        const _note = getLocalData(id);
        _note && setNote(_note);
        const _user = await getData({ id });
        _user && setUser(_user);
    }

    return (
        <div className="details-page">
            <div className="container">
                <div className="row">
                    <div className="col">
                        <div className="card">
                            <div className="card-header">
                                <i className="fa fa-user" aria-hidden="true" />
                                {user.name} ({user.username})
                            </div>
                            <div className="card-body">
                                <h5 className="card-title">Tracker notes</h5>

                                <div className="accordion mb-3" id="accordionPanelsStayOpenExample">
                                    {
                                        note?.data?.map((note: TrackerData, i: number) =>
                                            <div className="accordion-item" key={i}>
                                                <h2 className="accordion-header" id={`panelsStayOpen-heading${i}`}>
                                                    <button className="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target={`#panelsStayOpen-collapse${i}`} aria-expanded="true" aria-controls="panelsStayOpen-collapseOne">
                                                        <i className="fa fa-clock-o" aria-hidden="true" /> {note.amountTime} hours
                                                    </button>
                                                </h2>
                                                <div id={`panelsStayOpen-collapse${i}`} className="accordion-collapse collapse show" aria-labelledby={`panelsStayOpen-heading${i}`}>
                                                    <div className="accordion-body">
                                                        {note.notes}
                                                    </div>
                                                </div>
                                            </div>
                                        )
                                    }
                                </div>
                                <Link to={"/"}>
                                    <button type="button" className="btn btn-dark"> Back </button>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}