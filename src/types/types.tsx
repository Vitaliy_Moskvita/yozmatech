export enum Tab {
    Tracker = 'Tracker',
    List = 'List',
}

export type TrackerData = {
    userId: number | string,
    notes: string,
    amountTime: number | string
}

export type LocalData = {
    data: TrackerData[]
}

export type User = {
    id: number,
    name: string,
    username: string,
    email: string,
    address: {
        street: string,
        suite: string,
        city: string,
        zipcode: string,
        geo: {
            lat: string,
            lng: string
        }
    },
    phone: string,
    website: string,
    company: {
        name: string,
        catchPhrase: string,
        bs: string,

    }
}

export type RequestProps = {
    method?: string;
    body?: Object
    id?: string
}