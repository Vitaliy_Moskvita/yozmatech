import './App.css';
import { AdminPage } from './pages/admin';
import { Header } from './components/Header';
import { DetailsPage } from './pages/details';
import 'font-awesome/css/font-awesome.min.css';
import { Route, Switch, Redirect, withRouter } from "react-router-dom";


const App = () => {
  return (
    <div className="app">
      <Header />
      
      <Switch>
        <Route path='/admin' component={AdminPage} />
        <Route path='/details/:id' component={DetailsPage} />
        <Redirect from='/' to='/admin' />
      </Switch>
    </div>
  );
}

export default withRouter(App)
